/* TOP BUTTON */

var btn = $("#gotopbutton");

$(window).scroll(function () {
  if ($(window).scrollTop() > 300) {
    btn.addClass("show");
  } else {
    btn.removeClass("show");
  }
});

btn.on("click", function (e) {
  e.preventDefault();
  $("html, body").animate({ scrollTop: 0 }, "300");
});

/* MENU CLASS CANGE COLORS */

$(window).scroll(function () {
  var scroll = $(window).scrollTop();

  //>=, not <=
  if (scroll >= 300) {
    //clearHeader, not clearheader - caps H
    $(".mainMenu").addClass("floatingMenu");
  } else {
    $(".mainMenu").removeClass("floatingMenu");
  }
}); //missing );

/* STOP MODAL */

$(document).ready(function () {
  $("#youtubeVideo").on("hidden.bs.modal", function () {
    var $this = $(this).find("iframe"),
      tempSrc = $this.attr("src");
    $this.attr("src", "");
    $this.attr("src", tempSrc);
  });

  $("#html5Video").on("hidden.bs.modal", function () {
    var html5Video = document.getElementById("htmlVideo");
    if (html5Video != null) {
      html5Video.pause();
      html5Video.currentTime = 0;
    }
  });
});

$(document).ready(function () {
  new Splide(".slider-locales", {
    // focus: "center",
    type: "loop",
    perPage: 5,
    perMove: 1,
    page: 1,
    cover: false,
    // gap: 35,
    pagination: false,
    start: 1,
    breakpoints: {
      1024: {
        perPage: 4,
      },
      900: {
        perPage: 3,
      },
      600: {
        perPage: 2,
      },
      600: {
        perPage: 1,
        pagination: true,
      },
    },
  }).mount();

  new Splide(".slider-how-to", {
    // focus: "center",
    // type: "loop",
    perPage: 5,
    perMove: 1,
    page: 1,
    cover: false,
    gap: 20,
    pagination: false,
    start: 1,
    // arrows: false,
    rewind: true,
    breakpoints: {
      1100: {
        perPage: 4,
        pagination: true,
      },
      900: {
        perPage: 3,
        pagination: true,
      },
      700: {
        perPage: 2,
        pagination: true,
      },
      600: {
        perPage: 1,
        pagination: true,
      },
    },
  }).mount();
});

$("#videoRedlinks").on("hidden.bs.modal", function () {
  $(".yt-video").attr("src", "");
});

$(".play-btn").on("click", function () {
  $(".yt-video").attr("src", "https://www.youtube.com/embed/L4-DlCN-mhk");
});
