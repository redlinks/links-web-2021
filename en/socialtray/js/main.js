


$(document).ready(function(){
    $(".owl-carousel").owlCarousel({
        loop:true,
    margin:90,
    responsiveClass:true,
    responsive:{
        0:{
            items:3,
            nav:true
        },
        600:{
            items:4,
            nav:false,
            loop:true
        },
        1000:{
            items:5,
            nav:true,
            loop:true
        }
    }
    });
});

/* TOP BUTTON */

var btn = $('#gotopbutton');

$(window).scroll(function() {
  if ($(window).scrollTop() > 300) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});

btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});



/* MENU CLASS CANGE COLORS */

$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

     //>=, not <=
    if (scroll >= 300) {
        //clearHeader, not clearheader - caps H
        $(".mainMenu").addClass("floatingMenu");
    }
    else{
        $(".mainMenu").removeClass("floatingMenu");
    }
}); //missing );


