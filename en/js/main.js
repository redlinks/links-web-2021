



  
/* POP UP */
/*
$(window).on('load', function() {
  $('#popup').modal('show');
});
*/


/* ONE TIME SESSION POP UP */
/*  */


jQuery(document).ready(function(){

  // Start
  // sessionStorage.getItem('key');
  if (sessionStorage.getItem("visitorStory") !== 'true') {
    // sessionStorage.setItem('key', 'value'); pair
    sessionStorage.setItem("visitorStory", "true");
    // Calling the bootstrap modal
    $("#webinar").modal('show');
    } 
  // End
});

$(document).ready(function(){

    $("#carousel-services").owlCarousel({
        loop:true,
    margin:30,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:2,
            nav:false,
            loop:true
        },
        1000:{
            items:3,
            nav:true,
            loop:true
        }
    }
    });


    $('#carousel-testimonials').owlCarousel({
      loop:false,
      margin:70,
      nav:false,
      responsive:{
          0:{
              items:1
          },
          600:{
              items:2
          },
          1000:{
              items:3
          }
      }
    });
});



/* TOP BUTTON */

var btn = $('#gotopbutton');

$(window).scroll(function() {
  if ($(window).scrollTop() > 300) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});

btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});



/* MENU CLASS CANGE COLORS */

$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

     //>=, not <=
    if (scroll >= 300) {
        //clearHeader, not clearheader - caps H
        $(".mainMenu").addClass("floatingMenu");
    }
    else{
        $(".mainMenu").removeClass("floatingMenu");
    }
}); //missing );



/* STOP MODAL */

  $(document).ready(function() {
    $('#youtubeVideo').on('hidden.bs.modal', function() {
      var $this = $(this).find('iframe'),
        tempSrc = $this.attr('src');
      $this.attr('src', "");
      $this.attr('src', tempSrc);
    });
  
    $('#html5Video').on('hidden.bs.modal', function() {
      var html5Video = document.getElementById("htmlVideo");
      if (html5Video != null) {
        html5Video.pause();
        html5Video.currentTime = 0;
      }
    });
});
